<?php
include "database.php";
$db = new database();
?>
<?php
include 'include/head.php';
?>
<div class="row">
    <div class="col-md-12">                  
                       <div class="white-box">
                       <form action="proses/tambah_meja.php">
                           <h2 class="header-title">Meja </h2>
                           
                            <div class="table-responsive">
                             <table id="example" class="display table">
                                 <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Meja</th>
                                            <th>Status meja</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                     <tbody>
                                   <?php
                                        $no = 1;
                                        foreach ($db->tampil_meja() as $x) {
                                         
                                        ?>

                                        <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $x['no_meja']; ?></td>
                                        <td><?php echo $x['status_meja']; ?></td>
                                         <td>
                                              <a href="#" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $x['id_meja'];?>">Edit</a>                                                 
                                         
                                          
                                        </td>     </td>
                                      
                                        </tr>
                                   

                                    <?php } ?> 
                                    </tbody> 
                                   </table>
                                   </div>
                               </div>
                               </form>
                          </div>
</div>
              
<?php

include "../koneksi.php";
$no=0;
$data = "SELECT * from meja";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_meja']; 
$query_edit = mysqli_query($conn,"SELECT * FROM meja WHERE id_meja='$id'");
$r = mysqli_fetch_array($query_edit);
?>
          <div class="modal" id="myModal<?php echo $select_result['id_meja'];?>" role="dialog">
          <div class="modal-dialog">
           <div class="modal-content">

                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Meja</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_meja.php?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">

                    <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa  fa-pencil-square-o"></i></span>
                                      <input type="text" id="no_meja" class="form-control" placeholder="Masukkan No Meja" name="no_meja" value="<?php echo $r['no_meja']?>">
                                      </div>

                       <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa  fa-external-link"></i></span>
                                      <input type="text" id="status_meja" class="form-control" placeholder="Masukkan Status Meja" name="status_meja" value="<?php echo $r['status_meja']?>">
                                      </div>



                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                  </div>
                </div>    
                </div>                    


<?php } ?>



<?php
include 'include/footer.php';
?>