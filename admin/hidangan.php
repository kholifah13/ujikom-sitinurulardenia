<?php
include "database.php";
$db = new database();
?>

<?php
include 'include/head.php'
?>


        <!--body wrapper start-->
        <div class="wrapper"> 
           
               <!--Start row-->
                  
              <!--End Page Title-->          
           
           
               <!--Start row-->
               <div class="row">
                   <div class="col-md-12">
                       <div class="white-box">
                           <h2 class="header-title">Hidangan</h2>
                           <a data-toggle="modal" data-target="#myModal" class="btn btn-default"> Tambah</a>
                           
                           <div id="myModal" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                  </div>
                        <form action="proses/proses_makanan.php" method="POST" enctype="multipart/form-data" >
                                  
                                  <div class="modal-body">

                                      <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa fa-cutlery"></i></span>
                                      <input type="text" class="form-control" name="nama_masakan" placeholder="Nama Masakan">
                                      </div>

                                      <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa fa- fa-th-large"></i></span>
                                      <input type="text" class="form-control" name="kategori" placeholder="Kategori">
                                      </div>

                                      <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa  fa-money"></i></span>
                                      <input type="text" class="form-control" name="harga" placeholder="harga">
                                      </div>

                                      <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                      <input type="text" class="form-control" name="status_masakan" placeholder="Status">
                                      </div>

                                      <div class="input-group" style="padding-bottom: 10px">
                                      <span class="input-group-addon"><i class="fa fa-folder"></i></span>
                                      <input type="file" class="form-control" name="image" placeholder="gambar">
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                 
                                    <button type="submit" class="btn btn-default" >Tambah</button>
                                  </div>
                          </form>

                                </div>

                              </div>
                            </div>

                            <div class="col-md-12">
                       <div class="white-box">
                            <div class="table-responsive">
                             <table id="example" class="display" table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Hidangan</th>
                                            <th>Kategori</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                            <th>Gambar</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($db->tampil_data_masakan() as $x) {
                                         
                                        ?>

                                        <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $x['nama_masakan']; ?></td>
                                        <td><?php echo $x['kategori']; ?></td>
                                        <td><?php echo $x['harga']; ?></td>

                                        
                                          <td>
                                            <?php
                                            if($x['status_masakan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve_makanan.php?table=status_masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve_makanan.php?table=status_masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Tidak Tersedia
                                            </a>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                        <td><img src="gambar/<?php echo $x['image']; ?>" style="width: 10em;"></td>
                                        <td>
                                          <a href="#" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal<?php echo $x['id_masakan'];?>">Edit</a>
                                          
                                        </td>
                                        </tr>
                                    

                                    <?php } ?> 
                                    </tbody> 
                                        
                                    </table> 
								</div>
							</div>
							


 
 <?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from masakan";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_masakan']; 
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal" id="myModal<?php echo $select_result['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>

                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses/makanan_edit.php?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">
                     <div class="form-group" hidden="">
                                    <label for="nama_masakan">Nama Hidangan :</label>
                                      <input type="hidden" name="id_masakan" value="<?php echo $r['id_masakan']?>">
                                      <input type="text" id="nama_masakan" class="form-control" placeholder="Masukkan Nama Hidangan" name="nama_masakan" value="<?php echo $r['nama_masakan']?>">
                                </div>
                            <div class="input-group" style="padding-bottom: 10px">
                                    <label for="nama_masakan">Nama Hidangan :</label>
                                      <input type="hidden" name="nama_masakan" value="<?php echo $r['nama_masakan']?>">
                                      <input type="text" id="nama_masakan" class="form-control" placeholder="Masukkan Nama Hidangan" name="nama_masakan" value="<?php echo $r['nama_masakan']?>">
                                </div>
                                <div class="input-group" style="padding-bottom: 10px">
                                    <label for="kategori">Kategori :</label>
                                      <input type="text" class="form-control" name="kategori" placeholder="" value="<?php echo $r['kategori'] ?>">                                </div>
                                <div class="input-group" style="padding-bottom: 10px">
                                    <label for="harga">Harga :</label>
                                      <input type="harga" id="harga" class="form-control" placeholder="Masukkan harga yang Falid" name="harga" value="<?php echo $r['harga']?>">
                                </div>
                                 <div class="input-group" style="padding-bottom: 10px">
                                    <label for="status_masakan">Status Masakan :</label>
                                      <input type="status_masakan" id="status_masakan" class="form-control" placeholder="Masukkan Status Masakan" name="status_masakan" value="<?php echo $r['status_masakan']?>">
                                </div>
                                
                               <div class="input-group" style="padding-bottom: 10px">
                                    <label for="image">Image :</label>
                                      <input type="file" id="image" class="form-control" name="image" value="<?php echo $r['image']?>">
                                </div>
                                   
                                </div>
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php } ?>







                            </div>
                       </div>

                       </form>
                   </div>
               </div>
               <!--End row-->
               
			    </div>
          <!-- End Wrapper-->


       <?php
          include 'include/footer.php'
       ?>
</body>

</html>
