<?php
include "include/head.php";
?>
<?php
include'database.php';
$db = new database();
?>

<body>
	<div class="row">
		<div class="col-md-12">
                       <div class="white-box" style="width: 1100px; height: 500px;">
                       <form action="proses.php?aksi=tambah" method="post">
                          <h2 class="header-title">Tambah User</h2>
                            <div class="table-responsive">
                            <div class="card">
                             <?php
          include '../koneksi.php';
          $data_ambil_id_transaksi = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM transaksi where id_oder='$_GET[id_oder]'"));
          ?>
                        <div class="card-body">
            <?php
                $a = mysqli_query($conn,"SELECT keterangan_transaksi FROM transaksi WHERE id_oder='$_GET[id_oder]'");
                $b = mysqli_fetch_array($a);
                if($b[0]=="N"){
             ?>
                        	<button class="btn btn-success" type="submit" data-toggle="modal"
				             data-target="#mediumModal<?php echo $data_ambil_id_transaksi['id_transaksi'];?>"><i class="fa fa-shopping-cart"></i> Bayar
				           </button>
				           <?php } ?>
                             <table id="example" class="display table">
                             <thead>
                             	<tr>
                                        <th>No</th>
						                <th>Nama Masakan</th>
						                <th>Harga</th>
						                <th>Quantity</th>
						                <th>Keterangan</th>
						                <th>Status</th>
						                <th>Jumlah</th>
                                    </tr>
                             </thead>
                             
                             	     <?php
              $no = 1;
              $total=0;
             
              foreach($db->detail_tampil() as $x){
               $harga=$x['harga'];
                $jumlah=$x['jumlah']*$x['harga'];
                  $hasil="Rp.".number_format($harga,2,',','.');
                   $jml=$x['jml'] *$harga;
                    $jumlah1="Rp.".number_format($jml,2,',','.');
                ?>
					               <tbody>
					                <tr>
					                  <td><?php echo $no++; ?></td>
					                  <td><?php echo $x['nama_masakan']; ?></td>
					                  <td><?php echo $hasil; ?></td>
					                  <td><?php echo $x['jml']; ?></td>
					                  <td><?php echo $x['keterangan']; ?></td>
					                  <td><?php echo $x['status_detail_order']; ?></td>
					                  <td><?php echo $jumlah1;?></td>
					                </tr>
					              
					              <?php 
					              $total += ($jml) ;
					              $total1="Rp.".number_format($total,2,',','.');
					            }
					            ?>
					            <tr>
					              <td colspan="6" align="right">Total</td>
					              <td ><?php echo $total1;?></td>
					            </tr>
                             </tbody>
                             </table>
                             </div>
                         </div>    
                          </div>
                         </form>
              	 </div>
               </div>  
	</div>
</body>

<?php
	include 'include/footer.php';
	?>