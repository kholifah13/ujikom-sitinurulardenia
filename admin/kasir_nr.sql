DROP TABLE detail_oder;

CREATE TABLE `detail_oder` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_oder` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `status_detail_order` varchar(30) NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_oder`),
  KEY `id_masakan` (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO detail_oder VALUES("20","17","24","1","Manis","N");
INSERT INTO detail_oder VALUES("22","19","21","1","","menunggu pesanan");
INSERT INTO detail_oder VALUES("23","20","21","2","","menunggu pesanan");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","kasir");
INSERT INTO level VALUES("3","weiter");
INSERT INTO level VALUES("5","pelanggan");



DROP TABLE masakan;

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_masakan` varchar(100) NOT NULL,
  `kategori` varchar(8) NOT NULL,
  `harga` int(15) NOT NULL,
  `status_masakan` varchar(15) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Tersedia, Y Tersedia',
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id_masakan`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO masakan VALUES("21","nasi rebus","Makanan","15000","tersedia","img-35071-1-17e4942dec4804831dff65fb8764afe5_600x400.jpg");
INSERT INTO masakan VALUES("23","Ade Bakar","Minuman","15000","tersedia","w644.jpg");
INSERT INTO masakan VALUES("24","bubu","Makanan","55666","tersedia","c8f6aeb37197c4b76b20f7173172efb5.jpg");
INSERT INTO masakan VALUES("26","kuku kambing","Makanan","15000","tersedia","ayam sate.jpg");
INSERT INTO masakan VALUES("27","sate ayam","Makanan","25000","Tidak ters","ayam sate.jpg");
INSERT INTO masakan VALUES("28","susi","Makanan","10000","tersedia","logosmkn1ciomas.jpg");
INSERT INTO masakan VALUES("29","kucing","Minuman","10000","tersedia","loading-gears-animation-10.gif");



DROP TABLE meja;

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` int(3) NOT NULL,
  `status_meja` varchar(10) NOT NULL,
  PRIMARY KEY (`id_meja`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO meja VALUES("1","1","Y");
INSERT INTO meja VALUES("2","2","N");
INSERT INTO meja VALUES("3","3","N");
INSERT INTO meja VALUES("4","4","N");
INSERT INTO meja VALUES("5","5","N");
INSERT INTO meja VALUES("6","6","N");
INSERT INTO meja VALUES("7","7","Y");



DROP TABLE oder;

CREATE TABLE `oder` (
  `id_oder` int(11) NOT NULL AUTO_INCREMENT,
  `no_meja` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_oder` varchar(20) NOT NULL,
  PRIMARY KEY (`id_oder`),
  KEY `no_meja` (`no_meja`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO oder VALUES("17","7","2019-03-08 15:03:59","1","","Y");
INSERT INTO oder VALUES("19","2","2019-04-01 12:24:17","1","","Y");
INSERT INTO oder VALUES("20","1","2019-04-01 15:09:35","1","","Y");



DROP TABLE recovery_keys;

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO recovery_keys VALUES("1","1","30ca159a4a8075a420ec0a9c816ee4f9","1");
INSERT INTO recovery_keys VALUES("2","1","cd78b246105aa30a19e53ab2fa497d1e","0");
INSERT INTO recovery_keys VALUES("3","1","831c51f6aba2df3ed6095a5803fe816e","0");



DROP TABLE transaksi;

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_oder` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `keterangan_transaksi` varchar(20) NOT NULL,
  `uang` int(8) NOT NULL,
  `kembalian` int(8) NOT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_oder`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO transaksi VALUES("8","1","17","2019-03-08","55666","N","0","0");



DROP TABLE user;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nama_user` varchar(40) NOT NULL,
  `id_level` int(5) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif',
  PRIMARY KEY (`id_user`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("1","admin","21232f297a57a5a743894a0e4a801fc3","sitinurulkholifah13@gmail.com","admin","1","Y");
INSERT INTO user VALUES("2","kasir","c7911af3adbd12a035b289556d96470a","kasir@gmail.com","kasir","2","Y");
INSERT INTO user VALUES("3","yusi","f4fde24da0553f1853ecbaead47c7574","sitinurulkholifah13@gmail.com","yusi","3","Y");
INSERT INTO user VALUES("51","weiter","bf0dad671dd5176134c3f2cb353b6432","weiter@gmail.com","YFYF","3","Y");
INSERT INTO user VALUES("61","kiki","0d61130a6dd5eea85c2c5facfe1c15a7","kiki@gmail.com","kiki","2","N");
INSERT INTO user VALUES("74","davi","4aa606997465fd6fc4e825ff8695fcdf","davi@gmail.com","davi","2","N");
INSERT INTO user VALUES("75","owner","72122ce96bfec66e2396d2e25225d70a","owner@gmail.com","Rima","1","N");



