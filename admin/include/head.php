
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/images/favicon.png" type="image/png">
  <title>Zoo Resto</title>

    <link href="assets/plugins/morris-chart/morris.css" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
    <link href="assets/css/icons.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

      <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/datatables/css/jquery.dataTables-custom.css" rel="stylesheet" type="text/css"/>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sticky-header">


    <!--Start left side Menu-->
    <div class="left-side sticky-left-side">

        <!--logo-->
        <div class="logo">
            <a href="dashboard2.php"><img src="assets/images/logo.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="dashboard2.php"><img src="assets/images/logo-icon.png" alt=""></a>
        </div>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="menu"><a href="dashboard2.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                    
                </li>

                 <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Master Data</span></a>
                    <ul class="sub-menu-list">
                        <li  class="active"><a href="hidangan.php"> Data Masakan</a></li>
                        <li  class="active"><a href="user.php"> Data Pengguna</a></li>
                        <li  class="active"><a href="meja1.php">Data Meja</a></li>
                       
                    </ul>
                </li>
                 <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Pesan</span></a>
                  <ul class="sub-menu-list">
                        <li  class="active"><a href="meja.php"> Order Makanan</a></li>
                        <li  class="active"><a href="detail_order.php"> Order</a></li>
                       
                    </ul>
                    
                </li>
                <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Transaksi</span></a>
                  <ul class="sub-menu-list">
                        <li  class="active"><a href="sudah.php"> Transaksi Sudah</a></li>
                        <li  class="active"><a href="belum.php"> Transaksi Belum</a></li>
                       
                    </ul>          
                </li>
                
                 <li class="menu-list"><a href="#"><i class="icon-layers"></i> <span>Transaksi</span></a>
                  <ul class="sub-menu-list">
                        <li  class="active"><a href="sudah.php"> Transaksi Sudah</a></li>
                        <li  class="active"><a href="belum.php"> Transaksi Belum</a></li>
                       
                    </ul>
                    
                </li>
                

              <li class="menu"><a href="laporan.php"><i class="icon-envelope-open"></i> <span>Laporan</span></a>
                </li>
                 <li class="menu"><a href="backup.php"><i class="fa  fa-archive"></i> <span>Database</span></a>
                </li>

                <li ><a href="../logout.php"><i class="fa  fa-external-link"></i> <span>Logout</span></a>
                </li>

            </ul>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->
    
    
    <!-- main content start-->
    <div style="min-height: 700px;" class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <a class="toggle-btn"><i class="fa fa-bars"></i></a>

            

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                   
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/users/avatar-6.jpg" alt="" />
                             
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                          
                          <li> <a href="../logout.php"> <i class="fa fa-lock"></i> Logout </a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->