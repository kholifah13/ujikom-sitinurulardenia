<?php
include 'include/head.php';
?>


        <!--body wrapper start-->
        <div class="wrapper">
              
              <!--Start Page Title-->
               <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                   
                    <div class="clearfix"></div>
                 </div>
                  <!--End Page Title-->          
           

                  <!--Start row-->
                  <div class="row">
                      <div class="col-md-12">
                          <div class="row">
                          <div class="col-md-3">
                              <div class="analytics-box white-box">
                                  <h3>Total Revenue</h3>
                                  <div class="analytics-info">
                                       <div class="analytics-stats">
                                            $8659 
                                       </div>
                                       <span id="sparklinestats1"></span>
                                  </div>
                              </div>
                          </div> <!-- /analytics-box-->
 
 
                          <div class="col-md-3">
                              <div class="analytics-box white-box">
                                  <h3>Today Sales</h3>
                                  <div class="analytics-info">
                                       <div class="analytics-stats">
                                            $8659 
                                       </div>
                                       <span id="sparklinestats2"></span>
                                  </div>
                              </div>
                          </div> <!-- /analytics-box-->
 
                        
                          <div class="col-md-3">
                              <div class="analytics-box white-box">
                                  <h3>Total Visitors</h3>
                                  <div class="analytics-info">
                                       <div class="analytics-stats">
                                            6011 
                                       </div>
                                       <span id="sparklinestats3"></span>
                                  </div>
                              </div>
                          </div> <!-- /analytics-box-->
                        
                        
                          <div class="col-md-3">
                              <div class="analytics-box white-box">
                                  <h3>Bounce Rate</h3>
                                  <div class="analytics-info">
                                       <div class="analytics-stats">
                                            6011 
                                       </div>
                                       <span id="sparklinestats4"></span>
                                  </div>
                              </div>
                          </div> <!-- /analytics-box-->
                          
                         </div>                       
                      </div>
                  </div>
                  <!--End row-->
                  
           
           
                 <!--Start row-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="white-box">
                              <h2 class="header-title">Sales Statics</h2>
                            <!--Start chart-->
                              <div class="col-md-9"> 
                              <canvas id="sharpLinechart" height="90"></canvas>
                              </div>
                            <!--End chart-->  
                            
                            <div class="col-md-3">
                                    <div class="progress-main">
                                        <span class="progress-text">Total orders in period</span>
                                        <span class="progress-stats">48%</span>
                                        <div class="progress progress-sm">
                                            <div style="width: 48%;" class="progress-bar"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="progress-main">
                                        <span class="progress-text">Orders in last month</span>
                                        <span class="progress-stats">60%</span>
                                        <div class="progress progress-sm">
                                            <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="progress-main">
                                        <span class="progress-text">Monthly income</span>
                                        <span  class="progress-stats">22%</span>
                                        <div class="progress progress-sm">
                                            <div style="width: 22%;" class="progress-bar progress-bar-danger"></div>
                                        </div>
                                    </div>
                                   
                                </div>
                            
                         </div>
                     </div>
                 </div>
                 <!--End row-->
           
                  
              <!--Start row-->
              <div class="row">
             <!-- Start Feeds-->    
             <!-- End Feeds-->   
 
               
          </div>
          <!--End row-->
        </div>
        <!-- End Wrapper-->


        
    


    <?php
include 'include/footer.php';
    ?>

    
 </body>

</html>