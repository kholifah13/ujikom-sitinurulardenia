 <?php require_once("../koneksi.php");
    
    if (!isset($_SESSION)) {
        session_start();
    } ?>
<?php
include "include/head.php";
?>


<div class="row">
    
    <div class="col-md-6">
                 <div class="white-box">
                    <h2 class="header-title">Data Menu Makanan</h2>
                     <div class="table-wrap">
                            <table class="table">
                              <?php
                                    
                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data=mysqli_query($conn, "SELECT * FROM masakan where status_masakan='tersedia' ");
                                   
                                    while($show=mysqli_fetch_array($data)){
                                    ?>
   
            
          
              <div class="col-lg-3 col-xs-12">
                  <div class="white-box alert-box">
                    <p><?php echo $show['nama_masakan'];?></p>
                    <p><img style="width: 100px; height: 100px;" src='../admin/gambar/<?php echo $show['image']; ?>' ></p>
                    <p>Rp. <?php echo$show['harga'];?></p>
                    <button class="btn btn-primary" id="sa-basic"><a href ="cart.php?act=add&amp;id_masakan=<?php echo $show['id_masakan']; ?> &amp;ref=entri_order.php"><font color ="white">Pesan</font></a></button>
                </div>
              </div>
              <?php } ?>

                         
                         
                        </table>
                     </div>
                 </div>
                 </div>
               <!-- End Basic Table-->
                 
               <!-- Start  Striped  Table-->
                <form action="proses/pesan_makanan.php" method="post">
                <div class="col-md-6">
                 <div class="white-box">
                    <h2 class="header-title"> Striped Table</h2>
                    <?php
  if (isset($_SESSION['items1'])) {
      foreach ($_SESSION['items1'] as $key => $val) {
        $query = mysqli_query($conn, "SELECT * FROM meja WHERE id_meja = '$key'");
        $data = mysqli_fetch_array($query);
?>
<input type="hidden" name="no_meja" value="<?php echo $data['id_meja']?>;">
<?php
}
}
?>
                     <div class="table-wrap">
                            <table class="table table-striped">
                          <thead>
                            <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Harga</th>
                        <th>Quantity</th>
                        <th>Sub Total</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
                      $no = 1;
                      $total = 0;
    //mysql_select_db($database_conn, $conn);
                      if (isset($_SESSION['items'])) {
                          foreach ($_SESSION['items'] as $key => $val) {
                            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                            $data = mysqli_fetch_array($query);
                            $jumlah_barang = mysqli_num_rows($query);
                            $jumlah_harga = $data['harga'] * $val;
                            $total += $jumlah_harga;
                            $harga = $data['harga'];
                            $hasil = "Rp.".number_format($harga,2,',','.');
                            $hasil1 = "Rp.".number_format($jumlah_harga,2,',','.');
                            ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><input type="hidden" name="id_masakan[]" value="<?php echo $data['id_masakan']; ?>"><?php echo $data['nama_masakan']; ?></td>
                        <td><?php echo $hasil; ?></td>
                        <td><a href="cart.php?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a><input type="hidden" name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs <a href="cart.php?act=min&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a>
                        </td>
                        <td><?php echo $hasil1; ?></td>
                        <td><textarea class="form-control" name="keterangan[]"></textarea></td>
                        <td><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php">Cancel</a></td>
                      </tr>
                      <?php
                    //mysql_free_result($query);      
            }
              //$total += $sub;
            }?>
            <?php
        if($total == 0){ ?>
          <td colspan="4" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
        <?php } else { ?>
           <td colspan="6" style="font-size: 18px;"><b><div style="padding-right: 80px;" class="pull-right"><input type="hidden" value="<?php echo $total;?>" name="total_bayar">Total Harga Anda : Rp. <?php echo number_format($total); ?>,- </div> </b></td>
          
<?php 
}
?>
                          </tbody>
                        </table>
                        <p><div style="padding-right: 75px;" align="right">
            <button type="submit"  class="btn btn-success">&raquo; Konfirmasi Pemesanan &laquo;</a></button>
            </div></p>
                     </div>
                 </div>
                 </div>
                 </form>
               <!-- End  Striped Table-->


</div>
<?php
include 'include/footer.php';
?>