-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2019 at 11:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir_nr`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_oder`
--

CREATE TABLE `detail_oder` (
  `id_detail_order` int(11) NOT NULL,
  `id_oder` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `status_detail_order` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `detail_oder`
--

INSERT INTO `detail_oder` (`id_detail_order`, `id_oder`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(19, 16, 26, 1, 'sayang', 'menunggu pesanan'),
(20, 17, 24, 1, '', 'menunggu pesanan');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'kasir'),
(3, 'weiter');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL,
  `nama_masakan` varchar(100) NOT NULL,
  `kategori` varchar(8) NOT NULL,
  `harga` int(15) NOT NULL,
  `status_masakan` varchar(15) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Tersedia, Y Tersedia',
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `kategori`, `harga`, `status_masakan`, `image`) VALUES
(21, 'nasi rebus', 'Makanan', 15000, 'tersedia', 'img-35071-1-17e4942dec4804831dff65fb8764afe5_600x400.jpg'),
(23, 'Ade Bakar', 'Minuman', 15000, 'tersedia', 'w644.jpg'),
(24, 'bubu', 'Makanan', 55666, 'tersedia', 'c8f6aeb37197c4b76b20f7173172efb5.jpg'),
(26, 'kuku kambing', 'Makanan', 0, 'tersedia', 'ayam sate.jpg'),
(27, 'sate ayam', 'Makanan', 25000, 'Tidak ters', 'ayam sate.jpg'),
(28, 'susi', 'Makanan', 10000, 'tersedia', 'logosmkn1ciomas.jpg'),
(29, 'kucing', 'Minuman', 10000, 'tersedia', 'loading-gears-animation-10.gif');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL,
  `no_meja` int(3) NOT NULL,
  `status_meja` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(1, 1, 'Y'),
(2, 2, 'Y'),
(3, 3, 'N'),
(4, 4, 'N'),
(5, 5, 'N'),
(6, 6, 'N'),
(7, 7, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `oder`
--

CREATE TABLE `oder` (
  `id_oder` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_oder` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `oder`
--

INSERT INTO `oder` (`id_oder`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_oder`) VALUES
(16, 2, '2019-03-08 14:57:25', 1, '', 'Belum Dikonfirmasi'),
(17, 0, '2019-03-08 15:03:59', 1, '', 'Belum Dikonfirmasi');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 1, '30ca159a4a8075a420ec0a9c816ee4f9', 1),
(2, 1, 'cd78b246105aa30a19e53ab2fa497d1e', 0),
(3, 1, '831c51f6aba2df3ed6095a5803fe816e', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `status_transaksi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `status_transaksi`) VALUES
(7, 0, 16, '2019-03-08', 0, 'N'),
(8, 0, 17, '2019-03-08', 55666, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nama_user` varchar(40) NOT NULL,
  `id_level` int(5) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `email`, `nama_user`, `id_level`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'sitinurulkholifah13@gmail.com', 'admin', 1, 'Y'),
(2, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'kasir@gmail.com', 'kasir', 2, 'Y'),
(3, 'yusi', 'f4fde24da0553f1853ecbaead47c7574', 'sitinurulkholifah13@gmail.com', 'yusi', 3, 'N'),
(51, 'weiter', 'bf0dad671dd5176134c3f2cb353b6432', 'weiter@gmail.com', 'YFYF', 3, 'Y'),
(61, 'kiki', '0d61130a6dd5eea85c2c5facfe1c15a7', 'kiki@gmail.com', 'kiki', 2, 'N'),
(74, 'davi', '4aa606997465fd6fc4e825ff8695fcdf', 'davi@gmail.com', 'davi', 2, 'N'),
(75, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'owner@gmail.com', 'Rima', 1, 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_oder`
--
ALTER TABLE `detail_oder`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `id_order` (`id_oder`),
  ADD KEY `id_masakan` (`id_masakan`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `oder`
--
ALTER TABLE `oder`
  ADD PRIMARY KEY (`id_oder`),
  ADD KEY `no_meja` (`no_meja`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_oder`
--
ALTER TABLE `detail_oder`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oder`
--
ALTER TABLE `oder`
  MODIFY `id_oder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
